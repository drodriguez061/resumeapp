/**
 * Created by drodriguez on 18/1/2017.
 */
//importar config
var config = require('./config/config.json');
var express = require('express');
var bodyParser = require('body-parser');//Permite parsear JSON.
var methodOverride = require("method-override"); //Permite implementar y personalizar métodos HTTP.
var massive	= require("massive");//herramienta de acceso a datos PostgreSQL
var connectionString = "postgres://"+config.postgres.user+":"+config.postgres.password+"@"+config.postgres.host+"/"+config.postgres.db;
var massiveInstance = massive.connectSync({connectionString : connectionString});
//var app = express();
var app = module.exports = express();
var db;
app.set('db', massiveInstance);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(methodOverride());
var Usuarios = require('./controllers/usuarios');
var Contactos = require('./controllers/contactosusers')


var router = express.Router();

// Index
router.get('/', function(req, res) {
    res.send("Hello World - DevRod 06 Implements Api Rest using Node JS..!");
});

app.use(router);

// API routes
var api = express.Router();

api.route('/usuarios')
    .get(Usuarios.findAll)
    .post(Usuarios.save);

api.route('/usuario')
    .put(Usuarios.save);
api.route('/usuario/:id')
    .delete(Usuarios.delete);


//contactos
api.route('/contactos')
    .get(Contactos.findAll)
    .post(Contactos.save);
api.route('/contacto/:id')
    .get(Contactos.findOne)
    .delete(Contactos.delete);
api.route('/contacto/usuario/:id')
    .get(Contactos.userFind);

app.use('/api', api);
    //.post(ClientCtrl.add);
// Start server

var start = function () {
    app.listen(config.express.port);
    //db = app.get('db');
    console.log('_____________________');
    console.log('HTTP and API server online')
    console.log('Listening on port '+config.express.port);
    console.log('_____________________');
    console.log('running nodemon...');
}
var initialize = function () {
    start()
}


initialize()
