/**
 * Created by drodriguez on 20/1/2017.
 */
/**
 * Created by drodriguez on 19/1/2017.
 */
var app = require('./../app.js');
// ------------------------------------------
//			Send back a 500 error
// ------------------------------------------
var handleError	= function(res) {
    return function(err){
        console.log(err)
        res.send(500,{error: err.message});
    }
}
exports.findAll = function(request, res, next) {
    console.log('_____________________');
    console.log('API - list/usuarios');
    var db = app.get('db');
    // all active users
    if(!db.contactos){
        console.log('Log usuarios no found');
        return
    }

    db.usuarios.find(function(err, contactos){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        console.log('Suscces Completed!');
        res.json(contactos);
        // all users who are active
    });
};

exports.save = function (request, res, next) {
    var db = app.get('db');
    if(!db.contactos){
        console.log('This Object not Found');
        return
    }
    console.log(request.body);
    db.contactos.save(request.body, function(err, contactos) {
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        //full product with new id returned
        res.json(contactos);
    });

}

exports.delete = function (request, res, next) {
    var db = app.get('db');

    console.log('API - delete/usuario: '+request.params.id);
    db.contactos.destroy({id: request.params.id}, function(err, contactos){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        res.json(contactos);
    });

}
exports.findOne = function (request, res, next) {
    var db = app.get('db');
    console.log('API - find/contactos: '+request.params.id);
    db.contactos.find({id: request.params.id}, function(err, contactos){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        res.json(contactos);
    });
}

exports.userFind = function (request, res, next) {
    var db = app.get('db');
    console.log('API - find/contactos/usuario: '+request.params.id);
    db.contactos.find({usuario_id: request.params.id}, function(err, contactos){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        res.json(contactos);
    });
}


