/**
 * Created by drodriguez on 19/1/2017.
 */
var app = require('./../app.js');
// ------------------------------------------
//			Send back a 500 error
// ------------------------------------------
var handleError	= function(res) {
    return function(err){
        console.log(err)
        res.send(500,{error: err.message});
    }
}
exports.findAll = function(request, res, next) {
    console.log('_____________________');
    console.log('API - list/usuarios');
    var db = app.get('db');
    // all active users
    if(!db.usuarios){
        console.log('Log usuarios no found');
        return
    }

    db.usuarios.find(function(err, usuarios){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        console.log('Suscces Completed!');
        res.json(usuarios);
        // all users who are active
    });
};

exports.save = function (request, res, next) {
    var db = app.get('db');
    if(!db.usuarios){
        console.log('This Object not Found');
        return
    }
    console.log(request.body);
    db.usuarios.save(request.body, function(err, usuarios) {
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        //full product with new id returned
        res.json(usuarios);
    });

}

exports.delete = function (request, res, next) {
    var db = app.get('db');

    console.log('API - delete/usuario: '+request.params.id);
    db.usuarios.destroy({id: request.params.id}, function(err, usuarios){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        res.json(usuarios);
    });

}

exports.estudios = function(request, res, next) {
    console.log('_____________________');
    console.log('API - list/usuarios');
    var db = app.get('db');
    // all active users
    if(!db.estudio_usuario){
        console.log('Log usuarios no found');
        return
    }

    db.estudio_usuario(function(err, usuarios){
        if(err){
            console.log('Log error!');
            handleError(res)
        };
        console.log('Suscces Completed!');
        res.json(usuarios);
        // all users who are active
    });
};

/*module.exports = {
    getAll: function(request, res, next){
        console.log('_____________________');
        console.log('API - list/usuarios');
        var db = app.get('db');
        // all active users
        if(!db.usuarios){
            console.log('Log usuarios no found');
            return
        }
        if(err){
            console.log('Log error!');
            return
        };
        db.usuarios.find(function(err, usuarios){
            console.log('Suscces Completed!');
            res.json(usuarios);
            // all users who are active
        });
    }
}*/

