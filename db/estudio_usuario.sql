SELECT 
  usuarios.id, 
  usuarios.nombres, 
  usuarios.apellidos, 
  exptecnicas.id, 
  exptecnicas.estudio, 
  exptecnicas.descripcion
FROM 
  public.usuarios
LEFT OUTER JOIN 
  public.exptecnicas
ON 
  usuarios.id = exptecnicas.usuario_id;